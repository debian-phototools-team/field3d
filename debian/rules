#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#DH_VERBOSE = 1

DEB_HOST_ARCH_ENDIAN ?= $(shell dpkg-architecture --query DEB_HOST_ARCH_ENDIAN)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture --query DEB_HOST_MULTIARCH)

# Hardening flags.
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic
export DEB_CXXFLAGS_MAINT_APPEND = -Wall -pedantic
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# HDF5 1.14.x flags
export DEB_CFLAGS_MAINT_APPEND = -DH5_USE_110_API
export DEB_CXXFLAGS_MAINT_APPEND = -DH5_USE_110_API

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
		-DLIB_SUFFIX="/$(DEB_HOST_MULTIARCH)"
		-DINSTALL_DOCS=ON

# NOTE: The Ogawa feature introduced in 1.7.x is not supported for big-endian
# architectures. Upstream has yet to adapt the testsuite, so all we can do is
# disable testing if the host architecture is big-endian for now.
# See: https://github.com/imageworks/Field3D/issues/90
override_dh_auto_test:
ifneq (big,$(DEB_HOST_ARCH_ENDIAN))
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	cd obj-* && [ -x unitTest ] && ./unitTest
endif
endif

override_dh_install-indep:
	dh_install --indep
	dh_doxygen --indep

override_dh_installexamples-indep:
	dh_installexamples --indep --exclude=SCon*

override_dh_compress-indep:
	dh_compress --indep --exclude=examples

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: field3d
Upstream-Contact: Magnus Wrenninge <magnus.wrenninge@gmail.com>
Source: https://github.com/imageworks/Field3D

Files: *
Copyright: 2009-2015 Sony Pictures Imageworks Inc.
License: BSD-3-Clause

Files: debian/*
Copyright: 2016 Ghislain Antony Vaillant <ghisvail@gmail.com>
License: BSD-3-Clause

Files: export/Field3DFile.h
 export/MinMaxUtil.h
 export/OgawaFwd.h
 include/OgSparseDataReader.h
 src/Field3DFile.cpp
 src/FieldMetadata.cpp
 src/MinMaxUtil.cpp
Copyright: 2014 Sony Pictures Imageworks Inc.
 2014 Pixar Animation Studios Inc.
License: BSD-3-Clause

Files: include/All.h
 include/Foundation.h
 include/IArchive.h
 include/IData.h
 include/IGroup.h
 include/IStreams.h
 include/OArchive.h
 include/OData.h
 include/OGroup.h
 include/OStream.h
 include/UtilFoundation.h
 include/UtilPlainOldDataType.h
Copyright: 2013 Sony Pictures Inc.
 2013 Industrial Light & Magic, a division of Lucasfilm Entertainment Company Ltd.
License: BSD-3-Clause

Files: man/f3dinfo.1
Copyright: 2011 Magnus Wrenninge <magnus.wrenninge@gmail.com>
License: BSD-3-Clause

Files: test/misc_tests/lib_perf_test/src/main.cpp
Copyright: 2012 Sony Pictures Imageworks Inc.
 2012 DreamWorks Animation LLC
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
